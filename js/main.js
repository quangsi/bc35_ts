console.log("Hello ts");
// primitive value
var username = "alice";
username = "bob";
var age = 3;
var isHoliday = true;
var isMarried = null;
var is_married = undefined;
var sv1 = {
  id: 2,
  name: "tom",
  age: 2,
};
sv1.id = 4;
var todo1 = {
  id: 1,
  title: "Làm dự án cuối khoá",
  isCompleted: false,
};
var newTodo1 = {
  id: 1,
  title: "Làm capstone movie",
  desc: "Làm xong bỏ vào cv",
  isCompleted: false,
};
var sp1 = {
  id: 1,
  name: "tivi samsung",
  price: 10,
};
// type array
var numberArr = [2, 4, 6];
var foods = ["bún bò", "cơm tấm"];
var products = [
  sp1,
  {
    id: 2,
    name: "tủ lạnh",
    price: 20,
  },
];
var days = [4, 6, 7, 9];
// function type
function tinhTong(num1, num2) {
  return num1 + num2;
}
function handleClick(value) {
  console.log(value);
}
function main(callback, content) {
  callback(content);
}
function renderSection(content) {
  var contentHTML = `<p class="text-danger">Hello : ${content}</p>`;
  document.getElementById("root").innerHTML = contentHTML;
}
main(renderSection, "CyberSoft");
