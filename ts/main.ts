console.log("Hello ts");

// primitive value

var username: string = "alice";
username = "bob";
var age: number = 3;
var isHoliday: boolean = true;
var isMarried: null = null;
var is_married: undefined = undefined;

// reference value: object, array => interface
// interface mô trả định dạng dữ liệu của các key mà ojbect chứa
interface SinhVien {
  id: number;
  name: string;
  age: number;
}

var sv1: SinhVien = {
  id: 2,
  name: "tom",
  age: 2,
};
sv1.id = 4;
// sv1.id = "4";

interface Todo {
  id: number;
  title: string;
  isCompleted: boolean;
  time?: string; //optional property
}
var todo1: Todo = {
  id: 1,
  title: "Làm dự án cuối khoá",
  isCompleted: false,
};

interface NewTodo extends Todo {
  desc: string;
}

var newTodo1: NewTodo = {
  id: 1,
  title: "Làm capstone movie",
  desc: "Làm xong bỏ vào cv",
  isCompleted: false,
};

// type : dùng mô tả object, tạo ra type mới

type Product = {
  id: number;
  name: string;
  price: number;
};
var sp1: Product = {
  id: 1,
  name: "tivi samsung",
  price: 10,
};
// type array

var numberArr: number[] = [2, 4, 6];

var foods: string[] = ["bún bò", "cơm tấm"];

var products: Product[] = [
  sp1,
  {
    id: 2,
    name: "tủ lạnh",
    price: 20,
  },
];
var days: Array<number> = [4, 6, 7, 9];

// function type
function tinhTong(num1: number, num2: number): number {
  return num1 + num2;
}

function handleClick(value: number): void {
  console.log(value);
}

function main(callback: (title: string) => void, content: string): void {
  callback(content);
}

function renderSection(content: string) {
  var contentHTML = `<p>Hello : ${content}</p>`;
  document.getElementById("root").innerHTML = contentHTML;
}
main(renderSection, "CyberSoft");

// tuple ~ mảng hỗn hợp
var alice = {
  id: 1,
  name: "alice nguyen",
};

var user: [number, string] = [1, "alice nguyen"];

// union type
type ResponseAgeBE = number | string;

var userAge: ResponseAgeBE = "2";
// type any : chấp nhận mọi loại dữ liệu
let resultApi: any;
resultApi = "alice";
resultApi = 2;
resultApi = true;
